# Create a resource group if it doesn't exist
resource "azurerm_resource_group" "RG" {
  name     = "${var.environment}-RG"
  location = "Canada Central"

  tags = {
    environment = var.environment
  }
}

# Create virtual network
resource "azurerm_virtual_network" "VirtNET" {
  name                = "${var.environment}-VirtNET"
  address_space       = ["10.0.0.0/16"]
  location            = azurerm_resource_group.RG.location
  resource_group_name = azurerm_resource_group.RG.name

  tags = {
    environment = var.environment
  }
}

# Create subnet
resource "azurerm_subnet" "SubNET" {
  name                 = "testSubNET"
  resource_group_name  = azurerm_resource_group.RG.name
  virtual_network_name = azurerm_virtual_network.VirtNET.name
  address_prefixes     = ["10.0.1.0/24"]
}

# Create public IPs
resource "azurerm_public_ip" "PubIP" {
  name                = "${var.environment}-PubIP"
  location            = azurerm_resource_group.RG.location
  resource_group_name = azurerm_resource_group.RG.name
  allocation_method   = "Dynamic"

  tags = {
    environment = var.environment
  }
}

# Create Network Security Group and rule
resource "azurerm_network_security_group" "NSG" {
  name                = "${var.environment}-NSG"
  location            = azurerm_resource_group.RG.location
  resource_group_name = azurerm_resource_group.RG.name

  security_rule {
    name                       = "${var.environment}-SSH"
    priority                   = 1001
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "22"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }


  security_rule {
    name                       = "${var.environment}-VPN"
    priority                   = 1002
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Udp"
    source_port_range          = "*"
    destination_port_range     = "1194"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }

  tags = {
    environment = var.environment
  }
}



# Create network interface
resource "azurerm_network_interface" "NIC" {
  name                = "${var.environment}-NIC"
  location            = azurerm_resource_group.RG.location
  resource_group_name = azurerm_resource_group.RG.name

  ip_configuration {
    name                          = "${var.environment}-NICConf"
    subnet_id                     = azurerm_subnet.SubNET.id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          = azurerm_public_ip.PubIP.id
  }

  tags = {
    environment = var.environment
  }
}

# Connect the security group to the network interface
resource "azurerm_network_interface_security_group_association" "NSGonVM" {
  network_interface_id      = azurerm_network_interface.NIC.id
  network_security_group_id = azurerm_network_security_group.NSG.id
}

# Generate random text for a unique storage account name
resource "random_id" "randomId" {
  keepers = {
    resource_group = azurerm_resource_group.RG.name
  }
  byte_length = 8
}

# Create storage account for boot diagnostics
resource "azurerm_storage_account" "storageAccount" {
  name                     = "diag${random_id.randomId.hex}"
  resource_group_name      = azurerm_resource_group.RG.name
  location                 = azurerm_resource_group.RG.location
  account_tier             = "Standard"
  account_replication_type = "LRS"

  tags = {
    environment = var.environment
  }
}

# Create virtual machine
resource "azurerm_linux_virtual_machine" "VM" {
  name                  = "${var.environment}-${var.vm_computer_name}"
  location              = azurerm_resource_group.RG.location
  resource_group_name   = azurerm_resource_group.RG.name
  network_interface_ids = [azurerm_network_interface.NIC.id]
  size                  = "Standard_DS1_v2"

  os_disk {
    name                 = "${var.environment}-OSDisk"
    caching              = "ReadWrite"
    storage_account_type = "Premium_LRS"
    disk_size_gb         = "70"
  }

  source_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "18.04-LTS"
    version   = "latest"
  }

  computer_name                   = var.vm_computer_name
  admin_username                  = var.vm_user
  disable_password_authentication = true

  admin_ssh_key {
    username   = var.vm_user
    public_key = file(var.public_key)
  }

  boot_diagnostics {
    storage_account_uri = azurerm_storage_account.storageAccount.primary_blob_endpoint
  }

  tags = {
    environment = var.environment
  }
}