variable "environment" {
  type        = string
  description = "Environment name."
  default     = "test"
  validation {
    condition     = can(regex("[0-9A-Za-z-_]", var.environment))
    error_message = "Environment must contain only letters, numbers, underscores and/or dashes."
  }
}

variable "location" {
  type        = string
  description = "Location of RG."
  default     = "Canada Central"
  validation {
    condition     = can(regex("[A-Za-z-_ ]", var.location))
    error_message = "Resource name must contain only letters, whitespace and/or underscores and/or dashes."
  }
}

variable "public_key" {
  type        = string
  description = "The id of the machine image (AMI) to use for the server."
  default     = "~/.ssh/id_rsa.pub"
  validation {
    condition     = can(regex(".*\\.pub$", var.public_key))
    error_message = "The public key must end with \".pub\"."
  }
}

#https://unix.stackexchange.com/questions/157426/what-is-the-regex-to-validate-linux-users
variable "vm_user" {
  type        = string
  description = "VM default username for all instances."
  default     = "vps"
  validation {
    condition     = can(regex("^[a-z_]([a-z0-9_-]{0,31}|[a-z0-9_-]{0,30}\\$)$", var.vm_user))
    error_message = "The vm_user must contain only: \n  It should start (^) with only lowercase letters or an underscore ([a-z_]). This occupies exactly 1 character. \n    Then it should be one of either (( ... )): \n       From 0 to 31 characters ({0,31}) of letters, numbers, underscores, and/or hyphens ([a-z0-9_-]), OR (|) \n       From 0 to 30 characters of the above plus a USD symbol (\\$) at the end, and then \n    No more characters past this pattern ($)."
  }
}

variable "vm_computer_name" {
  type        = string
  description = "VM computer ame for all instances."
  default     = "linux"
  validation {
    condition     = can(regex("[[:alnum:]]", var.vm_computer_name))
    error_message = "The vm_computer_name must contain only letters and numbers."
  }
}